#Dockerfile

#Rubyのバージョン指定
FROM ruby:3.1.4

#Debianの公開鍵を引っ張ってきて格納、yarn.listにDebianのステーブル版を格納。残りはアップデート関連とnodejsとyarnのインストール
#RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \ && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \ && apt-get update -qq \ && apt-get install -y nodejs yarn

#ChatGPTさん正解
#apt-keyが非推奨でエラーが出るのでapt-keyのところをgpgコマンドで代用
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor -o /usr/share/keyrings/yarn-archive-keyring.gpg \
  && echo "deb [signed-by=/usr/share/keyrings/yarn-archive-keyring.gpg] https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list > /dev/null \
  && apt-get update -qq \
  && apt-get install -y nodejs yarn


#コンテナ側の作業ディレクトリ
WORKDIR /app

#ローカルのプロジェクト配下のsrc配下のファイル・ディレクトリをコンテナの/appにコピー
COPY ./src /app

#Gemのライブラリーをvedor/bunldeに格納。Gemfileに書かれているライブラリーをインストール
RUN bundle config --local set path 'vendor/bundle' \ && bundle install
#何故かこれ追加したら成功した(オリジナル)
#RUN bundle install
