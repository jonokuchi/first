class CreateTprs < ActiveRecord::Migration[7.1]
  def change
    create_table :tprs do |t|
      t.references :user, null: false, foreign_key: true
      t.decimal :business_time, precision: 4, scale: 2
      t.decimal :actual_driving, precision: 4, scale: 2
      t.decimal :pick_up_time, precision: 4, scale: 2
      t.decimal :waiting_time, precision: 4, scale: 2
      t.decimal :break_time, precision: 4, scale: 2
      t.decimal :backhaul_time, precision: 4, scale: 2
      t.decimal :average_operating_raito, precision: 3, scale: 2

      t.timestamps
    end
  end
end
