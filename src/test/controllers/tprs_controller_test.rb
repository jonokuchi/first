require "test_helper"

class TprsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tpr = tprs(:one)
  end

  test "should get index" do
    get tprs_url
    assert_response :success
  end

  test "should get new" do
    get new_tpr_url
    assert_response :success
  end

  test "should create tpr" do
    assert_difference("Tpr.count") do
      post tprs_url, params: { tpr: { actual_driving: @tpr.actual_driving, average_operating_raito: @tpr.average_operating_raito, backhaul_time: @tpr.backhaul_time, break_time: @tpr.break_time, business_time: @tpr.business_time, pick_up_time: @tpr.pick_up_time, user_id: @tpr.user_id, waiting_time: @tpr.waiting_time } }
    end

    assert_redirected_to tpr_url(Tpr.last)
  end

  test "should show tpr" do
    get tpr_url(@tpr)
    assert_response :success
  end

  test "should get edit" do
    get edit_tpr_url(@tpr)
    assert_response :success
  end

  test "should update tpr" do
    patch tpr_url(@tpr), params: { tpr: { actual_driving: @tpr.actual_driving, average_operating_raito: @tpr.average_operating_raito, backhaul_time: @tpr.backhaul_time, break_time: @tpr.break_time, business_time: @tpr.business_time, pick_up_time: @tpr.pick_up_time, user_id: @tpr.user_id, waiting_time: @tpr.waiting_time } }
    assert_redirected_to tpr_url(@tpr)
  end

  test "should destroy tpr" do
    assert_difference("Tpr.count", -1) do
      delete tpr_url(@tpr)
    end

    assert_redirected_to tprs_url
  end
end
