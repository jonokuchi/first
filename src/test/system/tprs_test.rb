require "application_system_test_case"

class TprsTest < ApplicationSystemTestCase
  setup do
    @tpr = tprs(:one)
  end

  test "visiting the index" do
    visit tprs_url
    assert_selector "h1", text: "Tprs"
  end

  test "should create tpr" do
    visit tprs_url
    click_on "New tpr"

    fill_in "Actual driving", with: @tpr.actual_driving
    fill_in "Average operating raito", with: @tpr.average_operating_raito
    fill_in "Backhaul time", with: @tpr.backhaul_time
    fill_in "Break time", with: @tpr.break_time
    fill_in "Business time", with: @tpr.business_time
    fill_in "Pick up time", with: @tpr.pick_up_time
    fill_in "User", with: @tpr.user_id
    fill_in "Waiting time", with: @tpr.waiting_time
    click_on "Create Tpr"

    assert_text "Tpr was successfully created"
    click_on "Back"
  end

  test "should update Tpr" do
    visit tpr_url(@tpr)
    click_on "Edit this tpr", match: :first

    fill_in "Actual driving", with: @tpr.actual_driving
    fill_in "Average operating raito", with: @tpr.average_operating_raito
    fill_in "Backhaul time", with: @tpr.backhaul_time
    fill_in "Break time", with: @tpr.break_time
    fill_in "Business time", with: @tpr.business_time
    fill_in "Pick up time", with: @tpr.pick_up_time
    fill_in "User", with: @tpr.user_id
    fill_in "Waiting time", with: @tpr.waiting_time
    click_on "Update Tpr"

    assert_text "Tpr was successfully updated"
    click_on "Back"
  end

  test "should destroy Tpr" do
    visit tpr_url(@tpr)
    click_on "Destroy this tpr", match: :first

    assert_text "Tpr was successfully destroyed"
  end
end
