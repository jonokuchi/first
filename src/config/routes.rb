Rails.application.routes.draw do
  resources :tprs
  devise_for :users
  #devise_for :users, skip: :recoverable
  # devise_for :users, except: "devise/passwords#new"

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  #仮のルートパス
  #root "interims#index"

  root "pub_users#index"

  #一般ユーザー用のルーティング
  get '/pub_users/', to: 'pub_users#index', as: "pub_users"
  #こちらでもOK(全てのルーティングが出る)
  #resources :pub_users

  # Defines the root path route ("/")
  # root "posts#index"
end
