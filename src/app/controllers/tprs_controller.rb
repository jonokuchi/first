class TprsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_tpr, only: %i[ show edit update destroy ]

  # GET /tprs or /tprs.json
  def index
    @tprs = Tpr.all
  end

  # GET /tprs/1 or /tprs/1.json
  def show
  end

  # GET /tprs/new
  def new
    @tpr = Tpr.new
  end

  # GET /tprs/1/edit
  def edit
  end

  # POST /tprs or /tprs.json
  def create
    @tpr = Tpr.new(tpr_params)

    respond_to do |format|
      if @tpr.save
        format.html { redirect_to tpr_url(@tpr), notice: "Tpr was successfully created." }
        format.json { render :show, status: :created, location: @tpr }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @tpr.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tprs/1 or /tprs/1.json
  def update
    respond_to do |format|
      if @tpr.update(tpr_params)
        format.html { redirect_to tpr_url(@tpr), notice: "Tpr was successfully updated." }
        format.json { render :show, status: :ok, location: @tpr }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @tpr.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tprs/1 or /tprs/1.json
  def destroy
    @tpr.destroy!

    respond_to do |format|
      format.html { redirect_to tprs_url, notice: "Tpr was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tpr
      @tpr = Tpr.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def tpr_params
      params.require(:tpr).permit(:user_id, :business_time, :actual_driving, :pick_up_time, :waiting_time, :break_time, :backhaul_time, :average_operating_raito)
    end
end
