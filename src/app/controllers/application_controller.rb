class ApplicationController < ActionController::Base
    before_action :configure_permitted_parameters, if: :devise_controller?

    protected

    #ストロングパラメーターの設定
    #新規登録(:sign_up)の時はデフォルトの値以外にnameとcompany_nameを受け付ける
    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :company_name])
    end

    #after_sign_up_path_forを使って新規登録後にtprsに遷移する
    def after_sign_up_path_for(resource)
        tprs_path
    end

    #after_sign_in_path_forメソッドを使ってログイン後にtprsにリダイレクトさせる
    def after_sign_in_path_for(resource)
        tprs_path
    end

    #after_sign_out_path_forメソッドを使ってログアウトしたらtprsに戻す
    #ログインしていないとtprsには行けないのでサインアップ画面に遷移する
    def after_sign_out_path_for(resource)
        tprs_path
    end
end
