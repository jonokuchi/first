class User < ApplicationRecord
  #Tprモデルの指定(Tprは多)
  has_many :tprs
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

         #パスワードのバリデーション最小文字を4文字最大を32にする
         #Deviseが提供するvalidatableモジュールにより、既にパスワードの最小・最大文字数の検証が行われているので不要
         #validates_length_of :encrypted_password, minimum: 4, maximum: 32

  #バリデーションメソッドでnameとemailに一意であることを宣言
  validates :name, presence: true, uniqueness: true
  validates :email, presence: true, uniqueness: true
end
