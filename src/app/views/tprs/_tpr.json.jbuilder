json.extract! tpr, :id, :user_id, :business_time, :actual_driving, :pick_up_time, :waiting_time, :break_time, :backhaul_time, :average_operating_raito, :created_at, :updated_at
json.url tpr_url(tpr, format: :json)
