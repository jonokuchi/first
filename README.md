
# タクシーの捕まえ易さを判断します     

### [機能一覧]
* 日時・気象条件などでお客様のタクシーの捕まえやすさを判定
* タクシー事業担当者のログイン機能
* タクシー事業担当者の輸送実績のCRUD機能

### [使用されている技術一覧]
- Ruby 3.1.4
- Rails 7.1.3 
  - 追加ライブラリ: devise(認証), dotenv(環境変数)
- Bundler 2.5.1
- Docker 24.0.2
- MySQL 8.0.35

### [fork,cloneした際に用意して頂くもの]
- .envファイル

参考とする.envファイルを用意しました。

### [参考とする.envファイル]
    MYSQL_ROOT_PASSWORD=secret
    MYSQL_DATABASE=traffics
    MYSQL_DB=traffics
    MYSQL_HOST=db
    MYSQL_USER=root  

### [.envファイルの配置]
    プロジェクトディレクトリ/first/src/.env

###  [操作の仕方]
プロフェクトディレクトリ/firstに移動して以下のコマンドを実行してください。
##  
    $ docker-compose up -d (コンテナ起動)
    $ docker-compose down --volumes (コンテナ停止・削除)

コンテナが起動しましたらWebブラウザで以下のURLを入力してタクシー捕まえやすさを判定します。
-  localhost:3000

タクシー会社担当者の新規登録・ログインのURLは以下の通りです。

-  localhost:3000/users/sign_up (新規登録)

-  localhost:3000/users/sign_in (ログイン)

＊ログインパスワードはログイン画面に記述

新規登録・ログイン後に輸送実績報告画面に移動します。ここでCRUD作業ができます。ここでの作業(営業時間や実車時間の入力)が、タクシー捕まえやすさの判定に影響します。

#  -------------English--------------------

#  It judges taxi getting easy

### [Feature List]
* Judging the ease of catching a taxi based on factors like date, time, and weather conditions.
* Login functionality for taxi business representatives.
* CRUD functionality for transportation records for taxi business representatives.

### [List of Technologies Used]
- Ruby 3.1.4
- Rails 7.1.3
  - Additional Libraries: 
    - devise (authentication), 
    - dotenv (environment variables)
- Bundler 2.5.1
- Docker 24.0.2
- MySQL 8.0.35

### [Things to Prepare When Forking or Cloning]
- .env file

A sample .env file has been provided for reference.

### [Sample .env File]
    MYSQL_ROOT_PASSWORD=secret
    MYSQL_DATABASE=traffics
    MYSQL_DB=traffics
    MYSQL_HOST=db
    MYSQL_USER=root  

### [Location of .env File]
    Project Directory/first/src/.env

### [How to Operate]
Navigate to the project directory/first and execute the following commands:
##  
    $ docker-compose up -d (Start containers)
    $ docker-compose down --volumes (Stop and remove containers)

Once the containers are running, use a web browser to assess the ease of catching a taxi at the following URL:
-  localhost:3000

For taxi company representatives, the URLs for new registration and login are as follows:

-  localhost:3000/users/sign_up (New Registration)
-  localhost:3000/users/sign_in (Login)

*The login password is indicated on the login screen.

After registration or login, you will be directed to the transportation records reporting page. Here, you can perform CRUD operations. The data entered here, including operational hours and actual driving time, will influence the assessment of taxi catchability.

#  --------------finish--------------------

